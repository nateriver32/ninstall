# ninstall


## Installation intructions

This will install Arch Linux with a preconfigured set of programs and settings. The initial script was designed
to be installed on bare metal, but it supports virtual machines as well. If you wish to use this environment please
make sure to read the documentation for info related to various  keybindings, settings and the overall modification
of the environment itself.

Before you begin please make sure you have a working internet connection either via wifi or ethernet. If you are
installing via wifi use the `iwctl` utility, heres a short guide to establish a reliable connection. Once you boot
into the live environment type the following:

```bash
iwctl
device list
station device scan
station device get-networks
station device connect <SSID>
```

After you establish a connection run the following commands:

```bash
curl -O https://gitlab.com/nateriver32/ninstall/-/raw/master/base.sh
bash base.sh
```

for a desktop install or:

```bash
curl -O https://gitlab.com/nateriver32/ninstall/-/raw/server/base.sh
bash base.sh
```

for a server install where the only noticeable difference is the number of packages installed.

Please set the values of the corresponding variables at the start of the `base.sh` and `rice-part-2.sh` files to accelerate
the installation.

Follow the instructions until the system reboots. After the reboot log into the created user and
run:

```bash
sudo bash rice-part-2.sh
```

Wait for the system to reboot. After rebooting, logging in again should spawn a minimal working environment.

## To-do list
- [ ] Make documentation for the environment
- [ ] Slim down server packages
