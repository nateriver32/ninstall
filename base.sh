#!/bin/bash

hostname=
root_password=
username=
password=
echo '
████████╗███████╗███████╗████████╗██╗███╗   ██╗ ██████╗     ██╗███╗   ██╗████████╗███████╗██████╗ ███╗   ██╗███████╗████████╗
╚══██╔══╝██╔════╝██╔════╝╚══██╔══╝██║████╗  ██║██╔════╝     ██║████╗  ██║╚══██╔══╝██╔════╝██╔══██╗████╗  ██║██╔════╝╚══██╔══╝
   ██║   █████╗  ███████╗   ██║   ██║██╔██╗ ██║██║  ███╗    ██║██╔██╗ ██║   ██║   █████╗  ██████╔╝██╔██╗ ██║█████╗     ██║   
   ██║   ██╔══╝  ╚════██║   ██║   ██║██║╚██╗██║██║   ██║    ██║██║╚██╗██║   ██║   ██╔══╝  ██╔══██╗██║╚██╗██║██╔══╝     ██║   
   ██║   ███████╗███████║   ██║   ██║██║ ╚████║╚██████╔╝    ██║██║ ╚████║   ██║   ███████╗██║  ██║██║ ╚████║███████╗   ██║   
   ╚═╝   ╚══════╝╚══════╝   ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═╝╚═╝  ╚═══╝   ╚═╝   ╚══════╝╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   
'
ping -c3 archlinux.org 1> /dev/null
sleep 0.1
if [ ! $? ]; then
  printf "\n Please connect to the internet before continuing\n"
  exit 1
else
  printf "\n Connection to the internet has been established.
 The installation may proceed.\n"
fi
sleep 0.1

echo '
██████╗ ██████╗ ███████╗ ██████╗ ██████╗ ███╗   ██╗███████╗██╗ ██████╗ ██╗   ██╗██████╗  █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
██╔══██╗██╔══██╗██╔════╝██╔════╝██╔═══██╗████╗  ██║██╔════╝██║██╔════╝ ██║   ██║██╔══██╗██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
██████╔╝██████╔╝█████╗  ██║     ██║   ██║██╔██╗ ██║█████╗  ██║██║  ███╗██║   ██║██████╔╝███████║   ██║   ██║██║   ██║██╔██╗ ██║
██╔═══╝ ██╔══██╗██╔══╝  ██║     ██║   ██║██║╚██╗██║██╔══╝  ██║██║   ██║██║   ██║██╔══██╗██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
██║     ██║  ██║███████╗╚██████╗╚██████╔╝██║ ╚████║██║     ██║╚██████╔╝╚██████╔╝██║  ██║██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
╚═╝     ╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
'
loadkeys us
sleep 0.1
timedatectl set-ntp true
sleep 0.1
timedatectl status
sleep 0.1

pacman -Sy --noconfirm fzf
printf '\n\n'
system=$(printf "UEFI\nBIOS\nVM" | fzf --height=10 --prompt="CHOOSE SYSTEM TYPE: ")
clear

echo '
██████╗ ██████╗ ██╗██╗   ██╗███████╗    ██████╗  █████╗ ██████╗ ████████╗██╗████████╗██╗ ██████╗ ███╗   ██╗
██╔══██╗██╔══██╗██║██║   ██║██╔════╝    ██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██║╚══██╔══╝██║██╔═══██╗████╗  ██║
██║  ██║██████╔╝██║██║   ██║█████╗      ██████╔╝███████║██████╔╝   ██║   ██║   ██║   ██║██║   ██║██╔██╗ ██║
██║  ██║██╔══██╗██║╚██╗ ██╔╝██╔══╝      ██╔═══╝ ██╔══██║██╔══██╗   ██║   ██║   ██║   ██║██║   ██║██║╚██╗██║
██████╔╝██║  ██║██║ ╚████╔╝ ███████╗    ██║     ██║  ██║██║  ██║   ██║   ██║   ██║   ██║╚██████╔╝██║ ╚████║
╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚══════╝    ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
'
lsblk
sleep 0.1
printf '\n\n'
sleep 0.1
drive=($(lsblk | awk '{print $1}' | grep -e '^sd' -e '^nvme' | fzf --height=7 --prompt="CHOOSE DRIVE TO INSTALL BASE SYSTEM ON: "))
wipefs -a /dev/${drive}
sleep 0.1
if [ "$system" = "BIOS" ]; then
  # (echo o; echo n; echo p; echo 1; echo ""; echo +4G; echo n; echo p; echo 2; echo ""; echo ""; echo t; echo 1; echo 82; echo t; echo 2; echo 83; echo a; echo 2; echo p; echo w) | fdisk /dev/${drive}
  (echo g; echo n; echo 1; echo ""; echo +1G; echo n; echo 2; echo ""; echo +4G; echo n; echo 3; echo ""; echo ""; echo t; echo 1; echo 4; echo t; echo 2; echo 19; echo t; echo 3; echo 23; echo p; echo w) | fdisk /dev/${drive}
else
  (echo g; echo n; echo 1; echo ""; echo +1G; echo n; echo 2; echo ""; echo +4G; echo n; echo 3; echo ""; echo ""; echo t; echo 1; echo 1; echo t; echo 2; echo 19; echo p; echo w) | fdisk /dev/${drive}
fi
sleep 0.1
if [[ ${drive} == *"nvme"* ]]; then
  drive="${drive}p"
fi
sleep 0.1

echo '
███████╗███╗   ██╗ ██████╗██████╗ ██╗   ██╗██████╗ ████████╗██╗███╗   ██╗ ██████╗ 
██╔════╝████╗  ██║██╔════╝██╔══██╗╚██╗ ██╔╝██╔══██╗╚══██╔══╝██║████╗  ██║██╔════╝ 
█████╗  ██╔██╗ ██║██║     ██████╔╝ ╚████╔╝ ██████╔╝   ██║   ██║██╔██╗ ██║██║  ███╗
██╔══╝  ██║╚██╗██║██║     ██╔══██╗  ╚██╔╝  ██╔═══╝    ██║   ██║██║╚██╗██║██║   ██║
███████╗██║ ╚████║╚██████╗██║  ██║   ██║   ██║        ██║   ██║██║ ╚████║╚██████╔╝
╚══════╝╚═╝  ╚═══╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝        ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝ 
'
if [[ "$system" == "BIOS" ]]; then
  # cryptsetup luksFormat /dev/${drive}3
  # cryptsetup luksOpen /dev/${drive}3 cryptroot
  echo "PASS"
else
  cryptsetup luksFormat /dev/${drive}3
  cryptsetup luksOpen /dev/${drive}3 cryptroot
fi
sleep 0.1

echo '
███╗   ███╗ █████╗ ██╗  ██╗██╗███╗   ██╗ ██████╗     ███████╗██╗██╗     ███████╗███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗███████╗
████╗ ████║██╔══██╗██║ ██╔╝██║████╗  ██║██╔════╝     ██╔════╝██║██║     ██╔════╝██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║██╔════╝
██╔████╔██║███████║█████╔╝ ██║██╔██╗ ██║██║  ███╗    █████╗  ██║██║     █████╗  ███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║███████╗
██║╚██╔╝██║██╔══██║██╔═██╗ ██║██║╚██╗██║██║   ██║    ██╔══╝  ██║██║     ██╔══╝  ╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║╚════██║
██║ ╚═╝ ██║██║  ██║██║  ██╗██║██║ ╚████║╚██████╔╝    ██║     ██║███████╗███████╗███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║███████║
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝╚══════╝
'
if [[ "$system" == "BIOS" ]]; then
  mkswap /dev/${drive}2
  sleep 0.1
  swapon /dev/${drive}2
  sleep 0.1
  mkfs.btrfs -f /dev/${drive}3
  sleep 0.1
  mount /dev/${drive}3 /mnt
  sleep 0.1
  cd /mnt
  sleep 0.1
  btrfs subvolume create @
  sleep 0.1
  btrfs subvolume create @home
  sleep 0.1
  cd
  sleep 0.1
  umount /mnt
  sleep 0.1
  mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@ /dev/${drive}3 /mnt
  sleep 0.1
  mkdir /mnt/home
  sleep 0.1
  mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home /dev/${drive}3 /mnt/home
else
  mkfs.fat -F32 /dev/${drive}1
  sleep 0.1
  mkswap /dev/${drive}2
  sleep 0.1
  swapon /dev/${drive}2
  sleep 0.1
  mkfs.btrfs /dev/mapper/cryptroot
  sleep 0.1
  mount /dev/mapper/cryptroot /mnt
  sleep 0.1
  cd /mnt
  sleep 0.1
  btrfs subvolume create @
  sleep 0.1
  btrfs subvolume create @home
  sleep 0.1
  cd
  sleep 0.1
  umount /mnt
  sleep 0.1
  mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@ /dev/mapper/cryptroot /mnt
  sleep 0.1
  mkdir /mnt/home
  sleep 0.1
  mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home /dev/mapper/cryptroot /mnt/home
  sleep 0.1
  mkdir /mnt/boot
  sleep 0.1
  mount /dev/${drive}1 /mnt/boot
fi

echo '
██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     ██╗███╗   ██╗ ██████╗     ██████╗  █████╗ ███████╗███████╗    ███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗
██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     ██║████╗  ██║██╔════╝     ██╔══██╗██╔══██╗██╔════╝██╔════╝    ██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║
██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     ██║██╔██╗ ██║██║  ███╗    ██████╔╝███████║███████╗█████╗      ███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║
██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     ██║██║╚██╗██║██║   ██║    ██╔══██╗██╔══██║╚════██║██╔══╝      ╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║
██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗██║██║ ╚████║╚██████╔╝    ██████╔╝██║  ██║███████║███████╗    ███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║
╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝    ╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝
'
sleep 0.1
reflector -c Croatia -a 6 --sort rate --save /etc/pacman.d/mirrorlist
sleep 0.1
pacman -Sy --noconfirm archlinux-keyring
sleep 0.1
pacman -Syyy
sleep 0.1
pacstrap -K /mnt base base-devel linux linux-firmware linux-headers sudo grub grub-btrfs efibootmgr dosfstools os-prober mtools networkmanager openssh cups keyd btrfs-progs cronie
sleep 0.1

echo '

 ██████╗ ███████╗███╗   ██╗███████╗██████╗  █████╗ ████████╗██╗███╗   ██╗ ██████╗     ███████╗██╗██╗     ███████╗███████╗██╗   ██╗███████╗████████╗███████╗███╗   ███╗
██╔════╝ ██╔════╝████╗  ██║██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██║████╗  ██║██╔════╝     ██╔════╝██║██║     ██╔════╝██╔════╝╚██╗ ██╔╝██╔════╝╚══██╔══╝██╔════╝████╗ ████║
██║  ███╗█████╗  ██╔██╗ ██║█████╗  ██████╔╝███████║   ██║   ██║██╔██╗ ██║██║  ███╗    █████╗  ██║██║     █████╗  ███████╗ ╚████╔╝ ███████╗   ██║   █████╗  ██╔████╔██║
██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║   ██║   ██║██║╚██╗██║██║   ██║    ██╔══╝  ██║██║     ██╔══╝  ╚════██║  ╚██╔╝  ╚════██║   ██║   ██╔══╝  ██║╚██╔╝██║
╚██████╔╝███████╗██║ ╚████║███████╗██║  ██║██║  ██║   ██║   ██║██║ ╚████║╚██████╔╝    ██║     ██║███████╗███████╗███████║   ██║   ███████║   ██║   ███████╗██║ ╚═╝ ██║
 ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝   ╚═╝   ╚══════╝   ╚═╝   ╚══════╝╚═╝     ╚═╝
'
genfstab -U -p /mnt >> /mnt/etc/fstab
sleep 0.1
cat /mnt/etc/fstab
sleep 0.1

echo '
███╗   ███╗ █████╗ ██╗  ██╗██╗███╗   ██╗ ██████╗     ███████╗██╗██████╗ ███████╗████████╗    ███████╗████████╗ █████╗  ██████╗ ███████╗
████╗ ████║██╔══██╗██║ ██╔╝██║████╗  ██║██╔════╝     ██╔════╝██║██╔══██╗██╔════╝╚══██╔══╝    ██╔════╝╚══██╔══╝██╔══██╗██╔════╝ ██╔════╝
██╔████╔██║███████║█████╔╝ ██║██╔██╗ ██║██║  ███╗    █████╗  ██║██████╔╝███████╗   ██║       ███████╗   ██║   ███████║██║  ███╗█████╗  
██║╚██╔╝██║██╔══██║██╔═██╗ ██║██║╚██╗██║██║   ██║    ██╔══╝  ██║██╔══██╗╚════██║   ██║       ╚════██║   ██║   ██╔══██║██║   ██║██╔══╝  
██║ ╚═╝ ██║██║  ██║██║  ██╗██║██║ ╚████║╚██████╔╝    ██║     ██║██║  ██║███████║   ██║       ███████║   ██║   ██║  ██║╚██████╔╝███████╗
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝   ╚═╝       ╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝
'
curl -L "https://gitlab.com/nateriver32/ninstall/-/raw/master/rice-part-1.sh" -o /mnt/root/rice-part-1.sh
sed -i "s/^drive=/drive=${drive}/" /mnt/root/rice-part-1.sh
sed -i "s/^system=/system=${system}/" /mnt/root/rice-part-1.sh
sed -i "s/^hostname=/hostname=${hostname}/" /mnt/root/rice-part-1.sh
sed -i "s/^root_password=/root_password=${root_password}/" /mnt/root/rice-part-1.sh
sed -i "s/^username=/username=${username}/" /mnt/root/rice-part-1.sh
sed -i "s/^password=/password=${password}/" /mnt/root/rice-part-1.sh
sleep 0.1
case $system in 
  "UEFI")
    # sed -i -e 's/^# mkdir \/boot\/EFI/mkdir \/boot\/EFI/' /mnt/root/rice-part-1.sh
    # sed -i -e 's/^# mount \/dev\/${drive}1 \/boot\/EFI/mount \/dev\/${drive}1 \/boot\/EFI/' /mnt/root/rice-part-1.sh
    sed -i '/^# grub-install --target=x86_64-efi --efi-directory=\/boot --bootloader-id=GRUB --recheck/s/^# //' /mnt/root/rice-part-1.sh;;
  "BIOS")
    sed -i '/^# grub-install --target=i386-pc/s/^# //' /mnt/root/rice-part-1.sh;;
  "VM")
    # sed -i -e 's/^# mkdir \/boot\/EFI/mkdir \/boot\/EFI/' /mnt/root/rice-part-1.sh
    # sed -i -e 's/^# mount \/dev\/${drive}1 \/boot\/EFI/mount \/dev\/${drive}1 \/boot\/EFI/' /mnt/root/rice-part-1.sh
    sed -i '/^# grub-install --target=x86_64-efi --efi-directory=\/boot --removable/s/^# //' /mnt/root/rice-part-1.sh;;
esac
sleep 0.1
chmod +x /mnt/root/rice-part-1.sh
sleep 0.1
sync
sleep 0.1
arch-chroot /mnt /root/rice-part-1.sh
sleep 0.1
rm -rf /mnt/root/rice-part-1.sh
sleep 0.1
umount -l /mnt
sleep 0.1
reboot
