#!/bin/bash
drive=
system=
hostname=
root_password=
username=
password=

echo '
██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     ██╗███╗   ██╗ ██████╗      ██████╗ ██████╗  █████╗ ██████╗ ██╗  ██╗██╗ ██████╗███████╗
██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     ██║████╗  ██║██╔════╝     ██╔════╝ ██╔══██╗██╔══██╗██╔══██╗██║  ██║██║██╔════╝██╔════╝
██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     ██║██╔██╗ ██║██║  ███╗    ██║  ███╗██████╔╝███████║██████╔╝███████║██║██║     ███████╗
██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     ██║██║╚██╗██║██║   ██║    ██║   ██║██╔══██╗██╔══██║██╔═══╝ ██╔══██║██║██║     ╚════██║
██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗██║██║ ╚████║╚██████╔╝    ╚██████╔╝██║  ██║██║  ██║██║     ██║  ██║██║╚██████╗███████║
╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝      ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝
'
pacman -S --noconfirm fzf
sleep 0.1
card=$(printf "AMD\nNVIDIA\nNONE" | fzf --height=10 --prompt="CHOOSE GRAPHICS CARD TYPE: ")

case $card in
  "AMD")
    pacman -Sy --noconfirm mesa;;
  "NVIDIA")
    pacman -Sy --noconfirm nvidia nvidia-utils nvidia-lts;;
  "NONE")
    ;;
esac
sleep 0.1

if [[ "$system" != "BIOS" ]]; then
echo '
 ██████╗ ███████╗███╗   ██╗███████╗██████╗  █████╗ ████████╗██╗███╗   ██╗ ██████╗     ██╗  ██╗ ██████╗  ██████╗ ██╗  ██╗███████╗
██╔════╝ ██╔════╝████╗  ██║██╔════╝██╔══██╗██╔══██╗╚══██╔══╝██║████╗  ██║██╔════╝     ██║  ██║██╔═══██╗██╔═══██╗██║ ██╔╝██╔════╝
██║  ███╗█████╗  ██╔██╗ ██║█████╗  ██████╔╝███████║   ██║   ██║██╔██╗ ██║██║  ███╗    ███████║██║   ██║██║   ██║█████╔╝ ███████╗
██║   ██║██╔══╝  ██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║   ██║   ██║██║╚██╗██║██║   ██║    ██╔══██║██║   ██║██║   ██║██╔═██╗ ╚════██║
╚██████╔╝███████╗██║ ╚████║███████╗██║  ██║██║  ██║   ██║   ██║██║ ╚████║╚██████╔╝    ██║  ██║╚██████╔╝╚██████╔╝██║  ██╗███████║
 ╚═════╝ ╚══════╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚══════╝
'
sed -E -i '/^MODULES/s/\(\)/\(btrfs\)/' /etc/mkinitcpio.conf
sed -E -i '/^HOOKS/s/block/block encrypt/' /etc/mkinitcpio.conf
mkinitcpio -p linux
fi

echo '
███████╗███████╗████████╗████████╗██╗███╗   ██╗ ██████╗     ███████╗ ██████╗ ███╗   ██╗███████╗██╗███╗   ██╗███████╗ ██████╗ 
██╔════╝██╔════╝╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝     ╚══███╔╝██╔═══██╗████╗  ██║██╔════╝██║████╗  ██║██╔════╝██╔═══██╗
███████╗█████╗     ██║      ██║   ██║██╔██╗ ██║██║  ███╗      ███╔╝ ██║   ██║██╔██╗ ██║█████╗  ██║██╔██╗ ██║█████╗  ██║   ██║
╚════██║██╔══╝     ██║      ██║   ██║██║╚██╗██║██║   ██║     ███╔╝  ██║   ██║██║╚██╗██║██╔══╝  ██║██║╚██╗██║██╔══╝  ██║   ██║
███████║███████╗   ██║      ██║   ██║██║ ╚████║╚██████╔╝    ███████╗╚██████╔╝██║ ╚████║███████╗██║██║ ╚████║██║     ╚██████╔╝
╚══════╝╚══════╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝╚═╝╚═╝  ╚═══╝╚═╝      ╚═════╝ 
'
region=$(ls -F /usr/share/zoneinfo/ | grep "/$" | sed '\/$/s/\///g' | fzf --height=10 --prompt="CHOOSE REGION: ")
sleep 0.1
city=$(ls /usr/share/zoneinfo/$region | fzf --height=10 --prompt="CHOOSE CITY: ")
sleep 0.1
ln -sf /usr/share/zoneinfo/$region/$city /etc/localtime
sleep 0.1
hwclock --systohc
sleep 0.1

echo '
███████╗███████╗████████╗████████╗██╗███╗   ██╗ ██████╗     ██╗      ██████╗  ██████╗ █████╗ ██╗     ███████╗
██╔════╝██╔════╝╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝     ██║     ██╔═══██╗██╔════╝██╔══██╗██║     ██╔════╝
███████╗█████╗     ██║      ██║   ██║██╔██╗ ██║██║  ███╗    ██║     ██║   ██║██║     ███████║██║     █████╗  
╚════██║██╔══╝     ██║      ██║   ██║██║╚██╗██║██║   ██║    ██║     ██║   ██║██║     ██╔══██║██║     ██╔══╝  
███████║███████╗   ██║      ██║   ██║██║ ╚████║╚██████╔╝    ███████╗╚██████╔╝╚██████╗██║  ██║███████╗███████╗
╚══════╝╚══════╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚══════╝ ╚═════╝  ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝
'
sed -i "/^#en_US.UTF-8/s/^#//" /etc/locale.gen
sleep 0.1
locale-gen
sleep 0.1

echo '
███████╗███████╗████████╗████████╗██╗███╗   ██╗ ██████╗     ██╗  ██╗ ██████╗ ███████╗████████╗███╗   ██╗ █████╗ ███╗   ███╗███████╗
██╔════╝██╔════╝╚══██╔══╝╚══██╔══╝██║████╗  ██║██╔════╝     ██║  ██║██╔═══██╗██╔════╝╚══██╔══╝████╗  ██║██╔══██╗████╗ ████║██╔════╝
███████╗█████╗     ██║      ██║   ██║██╔██╗ ██║██║  ███╗    ███████║██║   ██║███████╗   ██║   ██╔██╗ ██║███████║██╔████╔██║█████╗  
╚════██║██╔══╝     ██║      ██║   ██║██║╚██╗██║██║   ██║    ██╔══██║██║   ██║╚════██║   ██║   ██║╚██╗██║██╔══██║██║╚██╔╝██║██╔══╝  
███████║███████╗   ██║      ██║   ██║██║ ╚████║╚██████╔╝    ██║  ██║╚██████╔╝███████║   ██║   ██║ ╚████║██║  ██║██║ ╚═╝ ██║███████╗
╚══════╝╚══════╝   ╚═╝      ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═╝  ╚═╝ ╚═════╝ ╚══════╝   ╚═╝   ╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝
'
echo "$hostname" > /etc/hostname
sleep 0.1
echo "127.0.0.1     localhost" >> /etc/hosts
sleep 0.1
echo "::1           localhost" >> /etc/hosts
sleep 0.1
echo "127.0.1.1     $hostname.localdomain     $hostname" >> /etc/hosts
sleep 0.1
echo "root:$root_password" | chpasswd
sleep 0.1

echo '
 ██████╗██████╗ ███████╗ █████╗ ████████╗██╗███╗   ██╗ ██████╗     ██╗   ██╗███████╗███████╗██████╗ ███████╗
██╔════╝██╔══██╗██╔════╝██╔══██╗╚══██╔══╝██║████╗  ██║██╔════╝     ██║   ██║██╔════╝██╔════╝██╔══██╗██╔════╝
██║     ██████╔╝█████╗  ███████║   ██║   ██║██╔██╗ ██║██║  ███╗    ██║   ██║███████╗█████╗  ██████╔╝███████╗
██║     ██╔══██╗██╔══╝  ██╔══██║   ██║   ██║██║╚██╗██║██║   ██║    ██║   ██║╚════██║██╔══╝  ██╔══██╗╚════██║
╚██████╗██║  ██║███████╗██║  ██║   ██║   ██║██║ ╚████║╚██████╔╝    ╚██████╔╝███████║███████╗██║  ██║███████║
 ╚═════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝╚═╝  ╚═══╝ ╚═════╝      ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝╚══════╝
'
sleep 0.1
useradd -m $username
sleep 0.1
echo "$username:$password" | chpasswd
sleep 0.1
usermod -aG wheel,audio,video,optical,storage,lp,input $username
sleep 0.1
sed -i '/^# %wheel ALL=(ALL:ALL) ALL/s/^# //' /etc/sudoers
sleep 0.1
sed -i '/^#VerbosePkgLists/s/^#//' /etc/pacman.conf
sleep 0.1
sleep 0.1
sed -i '/^#Color/s/^#//' /etc/pacman.conf
sleep 0.1
sed -i '/ParallelDownloads/aILoveCandy' /etc/pacman.conf
sleep 0.1
mkdir /etc/systemd/system/getty@tty1.service.d
sleep 0.1
touch /etc/systemd/system/getty@tty1.service.d/autologin.conf
sleep 0.1
if [[ "$system" != "BIOS" ]]; then
echo -e "[Service] \nExecStart=\nExecStart=-/usr/bin/agetty --autologin $username --noclear %I 38400 linux" > /etc/systemd/system/getty@tty1.service.d/autologin.conf
fi
sleep 0.1
echo -e "blacklist pcspkr\nblacklist snd_pcsp" > /etc/modprobe.d/nobeep.conf
sleep 0.1
echo -e '[ids]\n*\n[main]\ncapslock = overloadt(capslock, esc, 125)\nrightshift = overloadt(meta, rightshift, 125)\n\n[capslock]\nh = left\nk = up\nj = down\nl = right\nf = f6\ne = f2\nd = delete' > /etc/keyd/default.conf
sleep 0.1
sed -i '/^#HandleLidSwitch/s/^#//' /etc/systemd/logind.conf
sleep 0.1
sed -i '/^#HandleLidSwitchExternalPower/s/^#//' /etc/systemd/logind.conf
sleep 0.1
sed -i '/^#HandleLidSwitchDocked/s/^#//' /etc/systemd/logind.conf
sleep 0.1
echo -e 'export QT_QPA_PLATFORMTHEME=qt6ct' > /etc/environment
sleep 0.1

echo '
███╗   ███╗ █████╗ ██╗  ██╗██╗███╗   ██╗ ██████╗     ██████╗  ██████╗  ██████╗ ████████╗██╗      ██████╗  █████╗ ██████╗ ███████╗██████╗ 
████╗ ████║██╔══██╗██║ ██╔╝██║████╗  ██║██╔════╝     ██╔══██╗██╔═══██╗██╔═══██╗╚══██╔══╝██║     ██╔═══██╗██╔══██╗██╔══██╗██╔════╝██╔══██╗
██╔████╔██║███████║█████╔╝ ██║██╔██╗ ██║██║  ███╗    ██████╔╝██║   ██║██║   ██║   ██║   ██║     ██║   ██║███████║██║  ██║█████╗  ██████╔╝
██║╚██╔╝██║██╔══██║██╔═██╗ ██║██║╚██╗██║██║   ██║    ██╔══██╗██║   ██║██║   ██║   ██║   ██║     ██║   ██║██╔══██║██║  ██║██╔══╝  ██╔══██╗
██║ ╚═╝ ██║██║  ██║██║  ██╗██║██║ ╚████║╚██████╔╝    ██████╔╝╚██████╔╝╚██████╔╝   ██║   ███████╗╚██████╔╝██║  ██║██████╔╝███████╗██║  ██║
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚═════╝  ╚═════╝  ╚═════╝    ╚═╝   ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚══════╝╚═╝  ╚═╝
'
if [[ "$system" == "BIOS" ]]; then
  id=$(blkid | grep ${drive}3 | awk '{print $2}' | sed 's/"//g')
  sed -i "/^GRUB_TIMEOUT/s/5/1/" /etc/default/grub
  # sed -i "/^GRUB_CMDLINE_LINUX_DEFAULT/s/loglevel=3 quiet/loglevel=3 cryptdevice=$id:cryptroot root=\/dev\/mapper\/cryptroot/" /etc/default/grub
else
  id=$(blkid | grep ${drive}3 | awk '{print $2}' | sed 's/"//g')
  sed -i "/^GRUB_TIMEOUT/s/5/1/" /etc/default/grub
  sed -i "/^GRUB_CMDLINE_LINUX_DEFAULT/s/loglevel=3 quiet/loglevel=3 cryptdevice=$id:cryptroot root=\/dev\/mapper\/cryptroot/" /etc/default/grub
fi
# mkdir /boot/EFI
# mount /dev/${drive}1 /boot/EFI
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB --recheck
# grub-install --target=i386-pc /dev/${drive}
# grub-install --target=x86_64-efi --efi-directory=/boot --removable
sleep 0.1
grub-mkconfig -o /boot/grub/grub.cfg
sleep 0.1

echo '
███╗   ███╗ █████╗ ██╗  ██╗██╗███╗   ██╗ ██████╗     ███████╗███████╗ ██████╗ ██████╗ ███╗   ██╗██████╗     ███████╗████████╗ █████╗  ██████╗ ███████╗
████╗ ████║██╔══██╗██║ ██╔╝██║████╗  ██║██╔════╝     ██╔════╝██╔════╝██╔════╝██╔═══██╗████╗  ██║██╔══██╗    ██╔════╝╚══██╔══╝██╔══██╗██╔════╝ ██╔════╝
██╔████╔██║███████║█████╔╝ ██║██╔██╗ ██║██║  ███╗    ███████╗█████╗  ██║     ██║   ██║██╔██╗ ██║██║  ██║    ███████╗   ██║   ███████║██║  ███╗█████╗  
██║╚██╔╝██║██╔══██║██╔═██╗ ██║██║╚██╗██║██║   ██║    ╚════██║██╔══╝  ██║     ██║   ██║██║╚██╗██║██║  ██║    ╚════██║   ██║   ██╔══██║██║   ██║██╔══╝  
██║ ╚═╝ ██║██║  ██║██║  ██╗██║██║ ╚████║╚██████╔╝    ███████║███████╗╚██████╗╚██████╔╝██║ ╚████║██████╔╝    ███████║   ██║   ██║  ██║╚██████╔╝███████╗
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝ ╚═════╝     ╚══════╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═════╝     ╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝
'
cd /home/$username
curl -O "https://gitlab.com/nateriver32/ninstall/-/raw/master/rice-part-2.sh"
sleep 0.1
curl -O "https://gitlab.com/nateriver32/ninstall/-/raw/master/packages-part-1.txt"
sleep 0.1
curl -O "https://gitlab.com/nateriver32/ninstall/-/raw/master/packages-part-2.txt"
sleep 0.1
chown $username:$username rice-part-2.sh
sleep 0.1
systemctl enable NetworkManager
sleep 0.1
systemctl enable sshd
sleep 0.1
systemctl enable cups
sleep 0.1
systemctl enable cronie
sleep 0.1
systemctl enable keyd
sleep 0.1
exit
